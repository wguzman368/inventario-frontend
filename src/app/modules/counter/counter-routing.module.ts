import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CounterListComponent } from './counter-list/counter-list.component';
import { CounterCreateComponent } from './counter-create/counter-create.component';
import { CounterUpdateComponent } from './counter-update/counter-update.component';
import { CounterViewComponent } from './counter-view/counter-view.component';


const routes: Routes = [
  {
    path:'counter-list',
    component:CounterListComponent
  },
  {
    path:'counter-create',
    component:CounterCreateComponent
  },
  {
    path:'counter-update',
    component:CounterUpdateComponent
  },
  {
    path:'counter-view',
    component:CounterViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CounterRoutingModule { }
