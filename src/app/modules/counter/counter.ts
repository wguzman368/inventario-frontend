export interface ICounter {

  id?:Number;
  equipment?:String;
  serie?:String;
  mark?:String;
  users?:String;
  buyDate?:String;
  price?:String;

}

export class Counter implements ICounter {

  id?:number;
  equipment?:String;
  serie?:String;
  mark?:String;
  users?:String;
  buyDate?:String;
  price?:String;
}
