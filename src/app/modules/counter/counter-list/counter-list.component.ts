import { Component, OnInit } from '@angular/core';
import { CounterService } from '../counter.service';
import { ICounter } from '../counter';

@Component({
  selector: 'app-counter-list',
  templateUrl: './counter-list.component.html',
  styleUrls: ['./counter-list.component.styl']
})
export class CounterListComponent implements OnInit {

  counter:ICounter[];

  constructor(private counterService:CounterService) { }

  ngOnInit(): void {
    this.counterService.query()
    .subscribe(res=>{
      console.warn('datos lista',res)
      this.counter=res;
    },error=>{
      console.warn('error',error)
    });
  }

}
