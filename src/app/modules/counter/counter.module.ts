import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CounterRoutingModule } from './counter-routing.module';
import { CounterCreateComponent } from './counter-create/counter-create.component';
import { CounterUpdateComponent } from './counter-update/counter-update.component';
import { CounterViewComponent } from './counter-view/counter-view.component';
import { CounterListComponent } from './counter-list/counter-list.component';


@NgModule({
  declarations: [CounterCreateComponent, CounterUpdateComponent, CounterViewComponent, CounterListComponent],
  imports: [
    CommonModule,
    CounterRoutingModule
  ]
})
export class CounterModule { }
