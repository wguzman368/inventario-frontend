import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICounter } from './counter';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CounterService {


  constructor(private http:HttpClient) { }

  public query(): Observable<ICounter[]>{
    return this.http.get<ICounter[]>(`${environment.END_POINT}/api/counter`)
    .pipe(map(res=>{
      return res;
    }));
  }
  }


