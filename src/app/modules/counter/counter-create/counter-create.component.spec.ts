import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterCreateComponent } from './counter-create.component';

describe('CounterCreateComponent', () => {
  let component: CounterCreateComponent;
  let fixture: ComponentFixture<CounterCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
