import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CounterListComponent } from './modules/counter/counter-list/counter-list.component';
import { CounterCreateComponent } from './modules/counter/counter-create/counter-create.component';
import { CounterUpdateComponent } from './modules/counter/counter-update/counter-update.component';
import { CounterViewComponent } from './modules/counter/counter-view/counter-view.component';


const routes: Routes = [
  {
    path:'counter',
    loadChildren:()=>import('./modules/counter/counter.module')
    .then(m=>m.CounterModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
